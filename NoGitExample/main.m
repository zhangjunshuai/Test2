//
//  main.m
//  NoGitExample
//
//  Created by mac on 15/11/10.
//  Copyright (c) 2015年 JS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
